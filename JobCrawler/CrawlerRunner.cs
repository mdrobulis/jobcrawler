﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobCrawler
{
    public class CrawlerRunner
    {

        public static void RunAll()
        {
            var cv = new CV_LT();
            var cb = new CvBankas();
            var cm = new CVMarket();
            var co = new CVOnline();



            //Console.WriteLine("------------------");
            //Console.WriteLine("------------------");
            //Console.WriteLine("Processing CV.LT ");
            //Console.WriteLine("------------------");
            //Console.WriteLine("------------------");


            //RunOne(cv);

            //Console.WriteLine("------------------");
            //Console.WriteLine("------------------");
            //Console.WriteLine("CV.LT Done");
            //Console.WriteLine("------------------");
            //Console.WriteLine("------------------");


            //RunOne(cb);

            //Console.WriteLine("------------------");
            //Console.WriteLine("------------------");
            //Console.WriteLine("CVBANKAS.LT Done");
            //Console.WriteLine("------------------");
            //Console.WriteLine("------------------");


            //RunOne(cm);

            //Console.WriteLine("------------------");
            //Console.WriteLine("------------------");
            //Console.WriteLine("CVMarket.LT Done");
            //Console.WriteLine("------------------");
            //Console.WriteLine("------------------");

            RunOne(co);

            Console.WriteLine("------------------");
            Console.WriteLine("------------------");
            Console.WriteLine("CVOnline.LT Done");
            Console.WriteLine("------------------");
            Console.WriteLine("------------------");


        }


        public static void RunOne(CvPage Page)
        {
            if (Page == null)
                return;

            else Page.ProcessTheWebsite();
        }


    }
}

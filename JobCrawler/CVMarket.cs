﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chapleau.MicrodataParser;
using HtmlAgilityPack;

namespace JobCrawler
{
    public class CVMarket : CvPage
    {
        public CVMarket()
        {
            WebsiteUrl = @"http://www.cvmarket.lt/joboffers.php?submit=1&categories={0}&start={1}"; 
        }

        protected override void ProcessContent(string content)
        {
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(content);
            var list = doc.DocumentNode.Descendants();
            
            var joblist2 = list.Where(x=> x.Attributes.Contains("class"));
            var joblist3 = joblist2.Where(x => x.Attributes["class"].Value.Contains("f_job_row"));

            if (joblist3.Count() == 0)
                return;
            
         
            foreach (var item in joblist3)
            {
                try
                {
                    AddJob(item);

                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.ToString());
                }
        }


            db.SaveChanges();

        }

        private void AddJob(HtmlNode item)
        {

            var children = item.Descendants();

            var xchild = children.Where(x => x.Attributes.Contains("class"));

            var job = new JobDescription();

            job.City = xchild.Where(x => x.Attributes["class"].Value.Contains("f_job_city")).FirstOrDefault().InnerText;
            job.Company = xchild.Where(x => x.Attributes["class"].Value.Contains("f_job_company")).FirstOrDefault().InnerText;

            job.Position = xchild.Where(x => x.Attributes["class"].Value.Contains("f_job_title")).FirstOrDefault().InnerText;
            job.URL = xchild.Where(x => x.Attributes["class"].Value.Contains("main_job_link")).FirstOrDefault().Attributes["href"].Value;

            HtmlNode date = xchild.Where(x => x.Attributes["class"].Value.Contains("col col5 faded")).FirstOrDefault();
            if(date !=null)
             job.DatePosted  = date.InnerText;


            if (xchild.Where(x => x.Attributes["class"].Value.Contains("f_job_salary")).Count() > 0)
                job.BaseSalary = xchild.Where(x => x.Attributes["class"].Value.Contains("f_job_salary")).FirstOrDefault().InnerText;


            Console.WriteLine(job);


            db.MyJobItems.Add(job);
        }




        public override void ProcessTheWebsite()
        {
            for (int categoryId = 2; categoryId < 20; categoryId++)
            {
                for (int skip = 0; skip < 300; skip += 25)
                {
                    string url = string.Format(WebsiteUrl, categoryId, skip);
                    ProcessThePage(url);
                }
            }
        }

        protected override void AddJob(MicrodataItem item)
        {
            throw new NotImplementedException();
        }
    }
}

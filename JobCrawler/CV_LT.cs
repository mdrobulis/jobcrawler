﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium;
using Chapleau.MicrodataParser;
using System.Net;
using System.IO;

namespace JobCrawler
{
    public class CV_LT :CvPage
    {
        //FirefoxDriver driver;

        

        
        //Parser metaParser;

        public CV_LT()
        {
            WebsiteUrl = "http://www.cv.lt/employee/announcementsAll.do?regular=true&ipp={0}page={1}";
            
        }

        public override void ProcessTheWebsite()
        {
            for (int i = 0; i < 10; i++)
            {
                ProcessThePage(string.Format(WebsiteUrl,100, i));
            }
        }

        protected override void AddJob(MicrodataItem item)
        {
            var job = new JobDescription()
            {

                City = item.Properties["jobLocation"],
                Company = item.Properties["hiringOrganization"],
                DatePosted = item.Properties["datePosted"],
                Position = item.Properties["title"],
                URL = item.Properties["url"],

            };
            if (item.Properties.ContainsKey("baseSalary"))
                job.BaseSalary = item.Properties["baseSalary"];

            //"datePosted"
            //"title"
            //"url"
            //"jobLocation"
            //"hiringOrganization"
            //"baseSalary"

            db.MyJobItems.Add(job);
        }
    }
}

# README #

This is a Crawler that collects all job postings on 4 major job hunting websites in Lithuania
* CV.lt
* CVOnline.lt
* CVBankas.lt
* CVMarket.lt

Basic info including a link to an actual job poster is stored to a local SQL Database for further use.

### Disclaimer ###

The project is using a slightly modified version of [Chapleau.MicrodataParser](https://microdata.codeplex.com/)



### What is this repository for? ###

* This project is for fun and profit.
* Version 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Pull the source from this REPO
* Compile the source
* Create a local MS SQL db named JobHunter with Windows Auth enabled (integrated security = true)
* Run the Console program
* Query your db full of Job opportunities


### Contribution guidelines ###

* Please make the code readable, if you don't know how - read first chapter of "Clean Code"
* Some parallel processing would give it a nice performance boost
* Downloading and indexing of job posting details would be of value ( after the first run you already have all the links)
* Tracking the tracking sessions would be a nice benefit.

### Who do I talk to? ###

* Repo owner is Martynas Drobulis <mdrobulis@gmail.com>
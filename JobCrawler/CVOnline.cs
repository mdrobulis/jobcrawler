﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chapleau.MicrodataParser;

namespace JobCrawler
{
    public class CVOnline:CvPage
    {
        public CVOnline()
        {
            WebsiteUrl = "http://www.cvonline.lt/darbo-skelbimai/visi?page={0}";
        }


        public override void ProcessTheWebsite()
        {
            for (int i = 0; i < 20; i++)
            {
                ProcessThePage(string.Format(WebsiteUrl, i));
            }
        }

        protected override void AddJob(MicrodataItem item)
        {
            var job = new JobDescription()
            {

                City = item.Properties["jobLocation"],
                Company = item.Properties["hiringOrganization"],
                DatePosted = item.Properties["datePosted"],
                Position = item.Properties["title"],
            //    URL = item.Properties["url"],

            };
            if (item.Properties.ContainsKey("baseSalary"))
                job.BaseSalary = item.Properties["baseSalary"];

            job.URL = Parser.ParserFirstUrl(item.ItemHTML);

            //"datePosted"
            //"title"
            //"url"
            //"jobLocation"
            //"hiringOrganization"
            //"baseSalary"

            db.MyJobItems.Add(job);
        }

        
    }
}

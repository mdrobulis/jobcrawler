namespace JobCrawler
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;

    public class JobDb : DbContext
    {
        // Your context has been configured to use a 'JobDb' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'JobCrawler.JobDb' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'JobDb' 
        // connection string in the application configuration file.
        public JobDb()
            : base("name=JobDb")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<JobDescription> MyJobItems { get; set; }
    }

    public class JobDetails
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        public string Description { get; set; }

        public string Requirements { get; set; }
        public string Benefits { get; set; }

        public string Details { get; set; }

        [ForeignKey("JobDescriptionID")]
        public JobDescription jobHeader;

    }

        public class JobDescription
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        public string Company { get; set; }
        public string City { get; set; }
        public string Position { get; set; }
        public int Views { get; set; }
        public string DatePosted { get; set; }

        public string BaseSalary { get; set; }

        public string URL { get; set; }

       

        //"datePosted"
        //"title"
        //"url"
        //"jobLocation"
        //"hiringOrganization"
        //"baseSalary"

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(Position);
            sb.AppendLine(Company);
            sb.AppendLine(City);
            sb.AppendLine(DatePosted);
            sb.AppendLine(BaseSalary);
            sb.AppendLine(URL);


            return sb.ToString();
        }

    }
}
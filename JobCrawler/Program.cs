﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobCrawler
{
    class Program
    {
        static void Main(string[] args)
        {
            CrawlerRunner.RunAll();

            Console.WriteLine("All Done");
            Console.Read();
        }
    }
}

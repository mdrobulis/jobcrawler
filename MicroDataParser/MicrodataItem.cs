﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chapleau.MicrodataParser
{
    public class MicrodataItem : MicrodataBase
    {
        public MicrodataItem()
        {
            ItemType = new List<string>();
            Properties = new Dictionary<string, string>();
        }

        public string ItemHTML { get; set; }

        public string ItemId { get; set; }
        public List<string> ItemType { get; set; }
        public Dictionary<string,string> Properties { get; set; }

        public override string ToJSON()
        {            
            List<string> itemList = new List<string>();
            if (!string.IsNullOrWhiteSpace(ItemId))
                itemList.Add("\"itemId\": \"" + ItemId + "\"");

            foreach(string itemType in ItemType)
                itemList.Add("\"itemType\": \"" + itemType + "\"");

            //foreach (var prop in Properties)
                //itemList.Add(prop.ToJSON());

            return "[" + Environment.NewLine + string.Join(",", itemList.ToArray()) + "]" + Environment.NewLine;
        }

    }
}

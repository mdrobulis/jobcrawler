﻿using Chapleau.MicrodataParser;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace JobCrawler
{
    public abstract class CvPage
    {
        protected string WebsiteUrl;

        protected JobDb db;

        public CvPage()
        {
            db = new JobDb();
        }

        public abstract void ProcessTheWebsite();



        public void ProcessThePage(string url)
        {
            string pageString = DownloadWebContentToFile(url);

            string content = File.ReadAllText(pageString);

            ProcessContent(content);

            
        }

        protected virtual void ProcessContent(string content)
        {
            

            List<MicrodataItem> metaList = Parser.Parse(content, false);

            foreach (MicrodataItem item in metaList)
            {

                try {
                    AddJob(item);
                    foreach (var prop in item.Properties)
                    {
                        Console.WriteLine(prop.Value);
                    }

                }
                catch (Exception ex) {

                    Console.WriteLine(ex.ToString());
                }

            }
            db.SaveChanges();
        }

        protected abstract void AddJob(MicrodataItem item);
        

        public string DownloadWebContentToFile(string url)
        {
            try
            {
                if (isNotContent(url))
                    return "";

                string file = Guid.NewGuid() + ".html";
                FileStream fs = new FileStream(file, FileMode.OpenOrCreate);


                HttpWebRequest r = WebRequest.CreateHttp(url);
                HttpWebResponse response = (HttpWebResponse)r.GetResponse();
                

                response.GetResponseStream().CopyTo(fs);

                fs.Close();

                return file;

            }
            catch (NotSupportedException ex)
            {
                // MailTO
                return "";
            }
            catch (WebException ex)
            {
                // 404
                return "";
            }
        }

        private bool isNotContent(string url)
        {
            string[] evilExt = { "jpg", "css", "png", "js" };
            foreach (string filter in evilExt)
                if (url.EndsWith(filter))
                    return true;
            return false;
        }

    }

    public interface ICvPage
    {
        void ProcessTheWebsite();
        void ProcessThePage(string url);
    }

}
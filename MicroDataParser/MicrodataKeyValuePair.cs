﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chapleau.MicrodataParser
{
    public class MicrodataKeyValuePair : MicrodataBase
    {
        public string Value { get; set; }

        public override string ToJSON()
        {
            return  "\"" + Name + "\": \"" + Value + "\"";
        }
    }
}

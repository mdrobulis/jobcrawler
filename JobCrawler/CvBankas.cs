﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chapleau.MicrodataParser;
using HtmlAgilityPack;

namespace JobCrawler
{
    public class CvBankas : CvPage
    {
        public CvBankas()
        {
            WebsiteUrl = "http://www.cvbankas.lt/?page={0}";
        }

        public override void ProcessTheWebsite()
        {
            for (int i = 1; i < 58; i++)
            {
                ProcessThePage(string.Format(WebsiteUrl, i));

            }
        }


        protected override void ProcessContent(string content)
        {
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(content);

            var topLevelItems = doc.DocumentNode.Descendants();
                        
            var ItemsFiltered = topLevelItems.Where(n => n.Name == "article");

            foreach(var item in ItemsFiltered)
            {
                try { 
                AddJob(item);
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.ToString());
                }
            }

            db.SaveChanges();
        }

        private void AddJob(HtmlNode item)
        {

            var children = item.Descendants();

            var job = new JobDescription();

                job.City = children.Where(x => x.Attributes.Contains("class")).Where(x => x.Attributes["class"].Value.Contains("list_city")).FirstOrDefault().InnerText.Trim();
                job.Company = children.Where(x => x.Attributes.Contains("class")).Where(x => x.Attributes["class"].Value.Contains("heading_secondary")).FirstOrDefault().InnerText.Trim();
                
                job.Position = children.Where(x => x.Attributes.Contains("class")).Where(x => x.Attributes["class"].Value.Contains("list_h3")).FirstOrDefault().InnerText.Trim();
                job.URL = children.Where(x => x.Name == "a").First().Attributes["href"].Value;

            if (children.Where(x => x.Attributes.Contains("class")).Where(x => x.Attributes["class"].Value.Contains("jobadlist_salary")).Count()>0)
                job.BaseSalary = children.Where(x => x.Attributes.Contains("class")).Where(x => x.Attributes["class"].Value.Contains("jobadlist_salary")).FirstOrDefault().InnerText.Trim();


            Console.WriteLine(job);


            db.MyJobItems.Add(job);
        }

        protected override void AddJob(MicrodataItem item)
        {
            throw new NotImplementedException();
        }
    }
}
